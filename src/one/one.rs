use utility::*;

fn hex_to_base64(hex: &[Hex]) -> Vec<Base64> {
    byte_array_to_base64(&hex_to_byte_array(hex))
}

#[test]
fn one() {
    assert_eq!(b"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t".to_vec(),
        hex_to_base64(b"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"));
}