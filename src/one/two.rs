use utility::*;
use crypto::xor as uxor;

fn xor(hex_a: &[Hex], hex_b: &[Hex]) -> Vec<Base64> {
    let a = hex_to_byte_array(hex_a);
    let b = hex_to_byte_array(hex_b);

    let ret = uxor(&a, &b);

    byte_array_to_hex(&ret)
}

#[test]
fn two() {
    assert_eq!(xor(b"1c0111001f010100061a024b53535009181c", b"686974207468652062756c6c277320657965"),
        b"746865206b696420646f6e277420706c6179".to_vec());
}