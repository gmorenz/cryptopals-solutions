#[test]
fn three() {
    use utility::*;

    let inp = b"1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    let val = hex_to_byte_array(inp);

    let res = find_english_xor_key(&val);

    let s: String = val.iter().map(|&x| (x ^ res) as char).collect();
    println!("{:?}", s);

    assert_eq!(&s, "Cooking MC\'s like a pound of bacon")
}