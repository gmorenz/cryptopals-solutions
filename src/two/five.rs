use std::collections::HashMap;

use utility::*;
use crypto::*;

lazy_static! {
    static ref KEY: Vec<u8> = rand_bytes(16);
}

fn parse(data: &[u8]) -> HashMap<Vec<u8>, Vec<u8>> {
    let mut ret = HashMap::new();
    data.split(|&c| c == b'&').map(|x| {
        let mut parts = x.split(|&c| c == b'=');
        let key = parts.next().unwrap().to_vec();
        let value = parts.next().unwrap().to_vec();
        assert!(parts.next().is_none());
        ret.insert(key, value);
    }).collect::<Vec<_>>();

    return ret;
}

fn profile_for(username: &[u8]) -> Vec<u8> {
    assert!(!username.contains(&b'&'));
    assert!(!username.contains(&b'='));
    let mut ret = vec![];
    ret.extend(b"email=");
    ret.extend(username);
    ret.extend(b"&uid=");
    // TODO: Maybe make this interesting
    // i.e. varying + varying in length
    ret.extend(b"10");
    ret.extend(b"&role=");
    ret.extend(b"user");
    ret
}

fn enc_user_profile(username: &[u8]) -> Vec<u8> {
    let mut profile = profile_for(username);
    pad(&mut profile, 16);
    ecb_aes128_encrypt(&profile, &KEY)
}

fn decrypt_profile(profile: &[u8]) -> HashMap<Vec<u8>, Vec<u8>> {
    let mut plaintext = ecb_aes128_decrypt(profile, &KEY);
    unpad(&mut plaintext);
    parse(&plaintext)
}

#[test]
fn five() {
    // Goal:
    //   Block 1: (with appropriate length email, can be stretched over multiple blocks)
    //     email=foo@bar.com&uid=10&role= // "email=" 6 bytes + "&uid=10&role=" 13 bytes = 19 byes, so use a 13 byte email.
    //   Block 2: email=789abcdef0admin<correct padding bytes 16 - 5 = 11 repeated>
    let email = b"morenzg@m.com";
    assert_eq!(email.len(), 13);
    let half1 = enc_user_profile(email);

    let fake_name = b"789abcdef0admin\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b";
    assert_eq!(fake_name.len(), 32 - "email=".len());
    let half2 = enc_user_profile(fake_name);

    let mut fakecipher = half1[..32].to_vec();
    fakecipher.extend(&half2[16..32]);

    let did_it_work = decrypt_profile(&fakecipher);
    assert_eq!(did_it_work[&b"role".to_vec()], b"admin");
}

#[test]
fn test_parse() {
    let str1 = b"foo=bar&baz=qux&zap=zazzle";
    let mut h1 = HashMap::new();
    h1.insert(b"foo".to_vec(), b"bar".to_vec());
    h1.insert(b"baz".to_vec(), b"qux".to_vec());
    h1.insert(b"zap".to_vec(), b"zazzle".to_vec());
    assert_eq!(h1, parse(str1));
}