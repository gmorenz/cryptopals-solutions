use utility::*;
use crypto::*;

fn encryption_oracle(input: &[u8]) -> (Vec<u8>, bool) {
    let key = rand_bytes(16);
    let prepad = rand_range(5, 10);
    let postpad = rand_range(5, 10);
    let mut plaintext = rand_bytes(prepad);
    plaintext.extend(input);
    plaintext.extend(rand_bytes(postpad));
    pad(&mut plaintext, 16);
    if coin() {
        let iv = rand_bytes(16);
        (cbc_aes128_encrypt(&plaintext, &iv, &key), true)
    }
    else {
        (ecb_aes128_encrypt(&plaintext, &key), false)
    }
}

#[test]
fn three() {
    let mut right = 0;
    for _ in 0..50 {
        let (ciphertext, is_cbc) = encryption_oracle(&[0; 48]);
        let prediction = ciphertext[16..32] != ciphertext[32..48];
        println!("{}, {}", is_cbc, prediction);
        right += if prediction == is_cbc {1} else {0};
    }
    assert!(right > 48);
}