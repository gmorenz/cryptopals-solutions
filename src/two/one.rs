#[test]
fn one() {
    use crypto::pad;

    let mut input = b"YELLOW SUBMARINE".to_vec();
    pad(&mut input, 20);
    assert_eq!(input, b"YELLOW SUBMARINE\x04\x04\x04\x04".to_vec());
}