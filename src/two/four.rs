use utility::*;
use crypto::*;


fn encryption_oracle(input: &[u8]) -> Vec<u8> {
    lazy_static! {
        static ref KEY: Vec<u8> = rand_bytes(16);
    }
    let mut plaintext = input.to_vec();
    plaintext.extend(DATA);
    pad(&mut plaintext, 16);
    ecb_aes128_encrypt(&plaintext, &KEY)
}

fn find_block_size() -> usize {
    for i in 1..0x7FFFFFFF {
        let out = encryption_oracle(&vec![0; 2*i]);
        if &out[0..i] == &out[i..2*i] {
            return i
        }
    }
    panic!("Probably not a ECB block cipher");
}

fn find_plaintext_len() -> usize {
    let len = encryption_oracle(&[]).len();
    let mut testtext = vec![];
    loop {
        testtext.push(0);
        let ciphertext = encryption_oracle(&testtext);
        if ciphertext.len() != len {
            return len - testtext.len();
        }
    }
}

#[test]
#[ignore]
fn four() {
    // Step 1
    let block_size = find_block_size();

    // Step 2
    let ciphertext = encryption_oracle(&vec![0; 3 * block_size]);
    assert_eq!(&ciphertext[0.. block_size],
            &ciphertext[block_size.. 2 * block_size]);

    // Find plaintext len (is there a better way?)
    let plaintext_len = find_plaintext_len();

    // Step 3 and onwards
    let len = encryption_oracle(&[]).len() + block_size;
    println!("len: {}", len);
    debug_assert!(len % block_size == 0);
    debug_assert!(block_size == 16);
    // len = 160
    // means actual = 144
    // means at least 128 bytes of data, at most 143. Actually 137.
    // We screw up on the padding :(
    let mut plaintext = vec!();
    for i in 0.. plaintext_len {
        let ciphertext = encryption_oracle(&vec![0; len - i - 1]);
        for x in 0..  {
            if x > 255 {
                panic! ("No x works?");
            }
            let x = x as u8;

            // faketext of the form [0's; known_plaintext; x]
            // with x as the last byte of a block
            let mut faketext = vec![0; len - i - 1];
            faketext.extend(&plaintext);
            faketext.push(x);

            let testtext = encryption_oracle(&faketext);

            let index = ((len - 1)/ block_size) * block_size;
            assert_eq!(faketext [index + block_size - 1], x);
            if &testtext [index .. index + block_size]
            == &ciphertext [index .. index + block_size] {
                println!("found x @ {}: {} ({})", i, x, x as char);
                plaintext.push (x);
                break
            }
        }
    }

    assert_eq!(&plaintext, &TARGET);
}

static DATA: &'static [u8] = base64!(
"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg\
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq\
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg\
YnkK");

static TARGET: &'static [u8] = b"Rollin' in my 5.0
With my rag-top down so my hair can blow
The girlies on standby waving just to say hi
Did you stop? No, I just drove by
";