use utility::*;
use crypto::*;

static mut LAST: u32 = 0;

fn encryption_oracle(input: &[u8]) -> Vec<u8> {
    lazy_static! {
        static ref KEY: Vec<u8> = rand_bytes(16);
    }
    let prepad = rand_range(5, 50);
    unsafe{ LAST = prepad };
    let mut plaintext = rand_bytes(prepad);
    plaintext.extend(input);
    plaintext.extend(DATA);
    pad(&mut plaintext, 16);
    ecb_aes128_encrypt(&plaintext, &KEY)
}

#[test]
fn six() {
    use std::cmp::max;

    // Find maximum length of random prepadding + target_text (a smarter algorithm would look at
    // how big the range is to choose how many times we need to sample).
    // The cipher text will be of the form <rand|pt_len padding|plaintext|padding>, padding at least 1
    // TODO: Put a justification for this magic formula
    let max_len = (0..16).map(|pt_len| {
        (0..500).map(|_| encryption_oracle(&vec![0; pt_len]).len()).max().unwrap() - pt_len - 1
    }).min().unwrap();
    assert_eq!(max_len, 49 + TARGET.len());

    let mut min_target_size = 0;
    for static_len in 0..16 {
        // Choose a varying length that makes it so we extend 1 byte into last possible block,
        // so we know we have a prepadding of maximal length.
        let var_len = 17 - ((max_len + static_len) % 16);
        let mut pt0 = vec![0; var_len];
        let mut pt1 = vec![1; var_len];
        pt0.extend(&vec![0; static_len]);
        pt1.extend(&vec![0; static_len]);

        let ct0;
        let mut k = 0;
        loop {
            k += 1;
            let potential_ct = encryption_oracle(&pt0);
            // println!("{} == {} + {} + 15", potential_ct.len(), max_len, pt0.len());
            if potential_ct.len() == max_len + pt0.len() + 15 {
                ct0 = potential_ct;
                assert_eq!(unsafe{ LAST }, 49);
                break;
            }
            if k > 200 {
                panic!("Infinite loop?");
            }
        }
        // panic!();
        let ct0_blocks: Vec<&[u8]> = ct0.chunks(16).collect();

        let ct1;
        loop {
            let potential_ct = encryption_oracle(&pt1);
            if potential_ct.len() == max_len + pt1.len() + 15 {
                ct1 = potential_ct;
                // assert_eq!(unsafe{ LAST }, 49);
                break;
            }
        }
        panic!();
        let ct1_blocks: Vec<&[u8]> = ct1.chunks(16).collect();

        // So we now have two encrypted plaintexts, of the same length, of the form
        // [stuff; stuff that definitely differs; stuff the same]
        // We want to know the length of "stuff the same", and from that derive the length of the target
        // We know that the last byte of stuff the same is at the start of a new block.

        assert_eq!(ct0_blocks.len(), ct1_blocks.len());
        assert_eq!(&ct0_blocks[&ct0_blocks.len() - 1], &ct1_blocks[ct1_blocks.len() - 1]);

        // Make loop pretty
        let mut same = 0;
        let mut i = ct0_blocks.len() - 1;
        while ct0_blocks[i] == ct1_blocks[i] {
            same += 1;
            if i == 0 {
                break;
            }
            else {
                i -= 1;
            }
        }

        min_target_size = max(min_target_size, same * 16 - static_len);
    }
    let target_size = min_target_size;
    assert_eq!(target_size, TARGET.len());
}


static DATA: &'static [u8] = base64!(
"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg\
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq\
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg\
YnkK");

static TARGET: &'static [u8] = b"Rollin' in my 5.0
With my rag-top down so my hair can blow
The girlies on standby waving just to say hi
Did you stop? No, I just drove by
";