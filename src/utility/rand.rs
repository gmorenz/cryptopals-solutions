extern crate rand as rand_crate;

use self::rand_crate::Rng;

pub fn rand_bytes(len: u32) -> Vec<u8> {
    let mut rng = rand_crate::os::OsRng::new().unwrap();
    let mut out = vec![0; len as usize];
    rng.fill_bytes(&mut out);
    out
}

pub fn rand_range(low: u32, high: u32) -> u32 {
    let mut rng = rand_crate::os::OsRng::new().unwrap();
    rng.gen_range(low, high)
}

pub fn coin() -> bool {
    let mut rng = rand_crate::os::OsRng::new().unwrap();
    rng.gen()
}