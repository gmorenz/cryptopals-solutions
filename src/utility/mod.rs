mod data_conv;
pub use self::data_conv::*;

mod heuristic;
pub use self::heuristic::*;

mod rand;
pub use self::rand::*;

pub fn find_english_xor_key(encoded: &[u8]) -> u8 {
    (0.. 256u16).max_by_key(|&i| {
        let s = encoded.iter().map(|&x| x ^ i as u8);
        is_english_heuristic(s)
    }).unwrap() as u8
}

pub fn repeated_xor(input: &[u8], key: &[u8]) -> Vec<u8> {
    input.iter().enumerate().map(|(i, &x)| x ^ key[i % key.len()]).collect()
}

#[test]
fn utility() {
    /*
    assert_eq!(
        base64_to_byte_array(
            &byte_array_to_base64(&[0x0f, 0x0ab, 0xde, 0xad])),
        vec![0x0, 0x0f, 0x0ab, 0xde, 0xad]);
    assert_eq!(base64_to_byte_array(b"YW55IGNhcm5hbCBwbGVhc3VyZS4"),
        b"any carnal pleasure.".to_vec());
    */

    assert_eq!(hamming_distance(b"this is a test", b"wokka wokka!!!"), 37);

    assert_eq!(repeated_xor(&[0,0,0,0], &[1,2]), vec![1, 2, 1, 2]);
    assert_eq!(repeated_xor(&[0,0,0,0,0], &[1,2]), vec![1, 2, 1, 2, 1]);
    assert_eq!(repeated_xor(&[1,2,1,2], &[1,2]), vec![0, 0, 0, 0]);
}