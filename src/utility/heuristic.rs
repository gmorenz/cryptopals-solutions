fn letter_frequency(x: u8) -> f64 {
    let c = (x as char).to_lowercase().next().unwrap_or(' ');
    match c {
        'a' => 8.12,
        'b' => 1.49,
        'c' => 2.71,
        'd' => 4.32,
        'e' => 12.02,
        'f' => 2.30,
        'g' => 2.03,
        'h' => 5.92,
        'i' => 7.31,
        'j' => 0.10,
        'k' => 0.69,
        'l' => 3.98,
        'm' => 2.61,
        'n' => 6.95,
        'o' => 7.68,
        'p' => 1.82,
        'q' => 0.11,
        'r' => 6.02,
        's' => 6.28,
        't' => 9.10,
        'u' => 2.88,
        'v' => 1.11,
        'w' => 2.09,
        'x' => 0.17,
        'y' => 2.11,
        'z' => 0.07,
        _ => 0.
    }
}

pub fn is_english_heuristic<I>(s: I) -> u64
where I: Iterator<Item=u8> {
    /*
    use std::ascii::AsciiExt;
    let mut len = 0.;
    let r: f64 = s.map(|c| {
        len += 1.;

        letter_frequency(c) +
        if c == b' ' { 25. } else { 0. } +
        if (c as char).is_ascii() { 100. } else { 0. }
    }).sum();

    (r * 1000. / len) as u64
    */
    s.map(|x| if (x >= b'A' && x <= b'Z') ||
                 (x >= b'a' && x <= b'z') ||
                 (x >= b'0' && x <= b'9') ||
                 x == b'.' ||
                 x == b'\n' ||
                 x == b'\'' ||
                 x == b' ' ||
                 x == b',' ||
                 x == b'!' ||
                 x == b';' ||
                 x == b':' ||
                 x == b'"' ||
                 x == b'?'
                { 1 } else { 0 }).sum()
}

pub fn hamming_distance(s1: &[u8], s2: &[u8]) -> u64 {
    s1.iter().zip(s2.iter())
        .map(|(&x, &y)| (x ^ y).count_ones() as u64)
        .sum()
}