// Types (currently) strictly for documentation purposes.
pub type Hex = u8;
pub type Base64 = u8;

#[test]
fn data_conv() {
    assert_eq!(hex_to_u8(b'9'), 9);
    assert_eq!(hex_to_u8(b'f'), 0xf);

    assert_eq!(u8_to_base64(1), b'B');
    assert_eq!(u8_to_base64(16), b'Q');
    assert_eq!(u8_to_base64(45), b't');
    assert_eq!(u8_to_base64(27), b'b');

    assert_eq!(hex_to_byte_array(b"ff0f"), vec![0xff, 0x0f]);
    assert_eq!(byte_array_to_hex(&[0x0f, 0xab]), b"0fab".to_vec());
}

fn round_up_div(a: usize, b: usize) -> usize {
    (a / b) + (if a % b == 0 { 0 } else { 1 })
}

fn hex_to_u8(a: Hex) -> u8 {
    if b'0' <= a && b'9' >= a {
        a - b'0'
    }
    else if b'a' <= a && b'f' >= a {
        a - b'a' + 10
    }
    else {
        panic!("Invalid hex value: {}", a)
    }
}

fn u8_to_hex(a: u8) -> Hex {
    if a < 10 {
        a + b'0'
    }
    else if a < 16 {
        a - 10 + b'a'
    }
    else {
        panic!("Invalid hex value: {}", a)
    }
}

fn u8_to_base64(a: u8) -> Base64 {
    if a < 26 {
        a + b'A'
    }
    else if a < 52 {
        a - 26 + b'a'
    }
    else if a < 62 {
        a - 52 + b'0'
    }
    else if a == 62 {
        b'+'
    }
    else if a == 63 {
        b'/'
    }
    else {
        panic!("Invalid value to convert to base64: {}", a)
    }
}

fn base64_to_u8(a: Base64) -> u8 {
    if b'A' <= a && a <= b'Z' {
        a - b'A'
    }
    else if b'a' <= a && a <= b'z' {
        a - b'a' + 26
    }
    else if b'0' <= a && a <= b'9' {
        a - b'0' + 52
    }
    else if a == b'+' {
        62
    }
    else if a == b'/' {
        63
    }
    else {
        panic!("Invalid base64 value: {}", a)
    }
}

pub fn hex_to_byte_array(hex: &[Hex]) -> Vec<u8> {
    let mut ret = vec![0; round_up_div(hex.len(), 2)];
    let mut acc = 0;
    let mut i = 0;

    // Shorthand, helps satisfy lexical borrow check issue
    let hl = hex.len();
    let rl = ret.len();

    while i < hl {
        let even = 0 == i % 2;
        acc += hex_to_u8(hex[hl - i - 1]) << if even {0} else {4};
        if !even {
            ret[rl - (i / 2) - 1] = acc;
            acc = 0;
        }

        i += 1;
    }

    if i % 2 == 1 {
        ret[0] = acc;
    }

    ret
}

pub fn byte_array_to_hex(val: &[u8]) -> Vec<Hex> {
    let mut ret = vec![0; val.len() * 2];
    for i in 0.. ret.len() {
        if i % 2 == 0 {
            ret[i] = u8_to_hex(val[i/2] >> 4);
        }
        else {
            ret[i] = u8_to_hex(val[i/2] & 0xf);
        }
    }

    ret
}

pub fn byte_array_to_base64(val: &[u8]) -> Vec<Base64> {
    let mut ret = vec![0; round_up_div(val.len() * 8, 6)];
    let mut acc: u16 = 0;
    let mut bits = 0;
    let mut ret_index = 0;

    let vl = val.len();
    let rl = ret.len();

    for i in 0.. vl {
        acc += (val[vl - i - 1] as u16) << bits;
        bits += 8;

        while bits >= 6 {
            ret[rl - ret_index - 1] = u8_to_base64((acc & 0x3f) as u8);
            ret_index += 1;
            acc = acc >> 6;
            bits -= 6;
        }
    }

    if ret_index < rl {
        assert!(ret_index + 1 == rl);
        ret[0] = u8_to_base64(acc as u8);
    }

    ret
}

/* Broken
pub fn base64_to_byte_array(base64: &[Base64]) -> Vec<u8> {
    let mut ret = vec![0; round_up_div(base64.len() * 6, 8)];
    let mut acc: u16 = 0;
    let mut bits = 0;
    let mut ret_index = 0;

    let bl = base64.len();
    let rl = ret.len();

    for i in 0.. bl {
        acc += (base64_to_u8(base64[bl - i - 1]) as u16) << bits;
        bits += 6;

        while bits >= 8 {
            ret[rl - ret_index - 1] = (acc & 0xff) as u8;
            ret_index += 1;
            acc = acc >> 8;
            bits -= 8;
        }
    }

    if ret_index < rl {
        assert!(ret_index + 1 == rl);
        ret[0] = acc as u8;
    }

    ret
}
*/
