// This crate exports no interface, it just tests itself
#![allow(dead_code)]
#![feature(plugin)]
#![plugin(binary_macros)]

#[macro_use] extern crate lazy_static;

mod utility;
mod crypto;

// mod one;
mod two;