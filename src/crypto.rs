extern crate crypto;

use self::crypto::aessafe::{AesSafe128Decryptor, AesSafe128Encryptor};
use self::crypto::symmetriccipher::{BlockDecryptor, BlockEncryptor};

pub fn ecb_aes128_decrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
    data.chunks(16)
        .flat_map(|x| aes128_decrypt(x, key).into_iter())
        .collect()
}

pub fn ecb_aes128_encrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
    data.chunks(16)
        .flat_map(|x| aes128_encrypt(x, key).into_iter())
        .collect()
}

pub fn cbc_aes128_encrypt(data: &[u8], iv: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(iv.len(), 16);
    data
        .chunks(16)
        .scan(iv.to_vec(),
            |state, x| {
                let res = aes128_encrypt(&xor(x, state), key);
                *state = res.clone();
                Some(res)
        })
        .flat_map(|x| x.into_iter())
        .collect()
}

pub fn cbc_aes128_decrypt(data: &[u8], iv: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(iv.len(), 16);
    data
        .chunks(16)
        .scan(iv,
            |state, x| {
                let res = xor(*state, &aes128_decrypt(x, key));
                *state = x;
                Some(res)
        })
        .flat_map(|x| x.into_iter())
        .collect()
}

pub fn aes128_decrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(data.len(), 16);
    assert_eq!(key.len(), 16);
    let mut output = vec![0; 16];
    AesSafe128Decryptor::new(key).decrypt_block(data, &mut output);
    output
}

pub fn aes128_encrypt(data: &[u8], key: &[u8]) -> Vec<u8> {
    assert_eq!(data.len(), 16);
    assert_eq!(key.len(), 16);
    let mut output = vec![0; 16];
    AesSafe128Encryptor::new(key).encrypt_block(data, &mut output);
    output
}

pub fn pad(data: &mut Vec<u8>, block_size: usize) {
    use std::iter::repeat;

    let padding = block_size - (data.len() % block_size);
    assert!(padding < 256);

    data.extend(repeat(padding as u8).take(padding));
}

pub fn unpad(data: &mut Vec<u8>) {
    let padding = data[data.len() - 1];
    for _ in 0.. padding{
        let r = data.pop();
        debug_assert!(r == Some(padding));
    }
}

pub fn xor(a: &[u8], b: &[u8]) -> Vec<u8> {
    assert_eq!(a.len(), b.len());
    a.into_iter().zip(b.into_iter()).map(|(x, y)| x ^ y).collect()
}

#[test]
fn test_aes() {
    let msg = b"YELLOW SUBMARINE";
    let key = b"YELLOW SUBMARINE";

    assert_eq!(b"YELLOW SUBMARINE".to_vec(),
        aes128_decrypt(&aes128_encrypt(msg, key), key));
}

#[test]
fn test_pad() {
    let msg = b"YELLOW SUBMARINE".to_vec();
    let mut msg_pad = msg.clone();
    pad(&mut msg_pad, 20);
    assert_eq!(msg_pad, b"YELLOW SUBMARINE\x04\x04\x04\x04".to_vec());
    unpad(&mut msg_pad);
    assert_eq!(msg, msg_pad);
}